## PM2.0 框架设计与开发

## 需求

目前的PM组件，使用起来，有以下的问题点：

* PM Device注册机制不太友好，pm device notify:all suspend/resume机制无法考虑场景。
* request/release接口无模块ID的划分，无法清除是哪个线程request
* 初始化电源模式有问题，如果没有request，应该进入深睡眠
* request应该是请求【不睡眠】，但request(DEEPSLEEP)马上进入睡眠
* 外设延时睡眠功能的操作不好实现

## 变更
* PM Device注册机制保留，不推荐使用，考虑兼容性
* 丰富request/release接口，增加module_id
* 初始化时的电源模式，依旧有引用计数，无request则进入深睡眠【DEEPSLEEP】
* 不建议用户request(DEEPSLEEP)，若没有任何request,自动进入DEEPSLEEP。
* 增加延时睡眠的接口，方便用户延时睡眠的操作。

## 说明
* 低功耗定时器的管理，如线程delay执行的操作，进入DEEPSLEEP后，也会唤醒继续执行。
* 变频，最好独立出去，因为变频牵涉到很多外设的正常执行。
* PM Device不建议使用。

## 使用方法

* 硬件平台：Pandora STM32L475开发板，或STM32L4系列类似平台
* 开发IDE:Keil MDK5
* RT-Thread ENV工具
* 工程地址：demo\bsp\stm32\stm32l475-atk-pandora\project.uvprojx


## 操作步骤

* 进入 demo\bsp\stm32\stm32l475-atk-pandora 目录
* ENV工具：menuconfig
* ENV工具：scons --target=mdk5
* 打开新生成Keil工程:demo\bsp\stm32\stm32l475-atk-pandora\project.uvprojx

## 功能

* 上电后，为NONE模式，需要睡眠时，代码实现或是msh命令 pm_release 0
* 按键测试睡眠与唤醒逻辑
* LPUART STOP2唤醒
* 建议采用新的PM API接口，用于灵活的功耗管理

```
pm_module_request(PM_BOARD_ID, PM_SLEEP_MODE_IDLE); //请求不睡眠，可以工作在 NONE、IDLE模式
pm_module_release(PM_BOARD_ID, PM_SLEEP_MODE_IDLE); //操作完，允许睡眠，释放请求
pm_module_release_all(PM_BOARD_ID, PM_SLEEP_MODE_IDLE); //释放指定睡眠模式的所有的请求.

rt_pm_module_delay_sleep(PM_BOARD_ID, 1000); //如等待外设初始化完成，延时1S再睡眠
 ```

## 其他文件

* pms.c ： 按键测试
* lpuart_test.c lpuart唤醒


## 异常分析

* 开启PM组件后，需要手动增加idle线程的栈大小，如改为2048

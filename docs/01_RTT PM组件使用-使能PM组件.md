# RT-Thread PM组件移植与使用-使能PM组件

<br>

## 目的

使用RT-Thread PM（电源管理）组件，更好的管理系统的功耗。

<br>


## 使能组件

RT-Thread的PM，默认在IDLE线程调用。需要打开：RT_USING_PM。   

这里使用RT-Thread提供的ENV工具，通过menuconfig图形界面的方式，使能PM组件。   

<br>

## 准备

* 开发板：潘多拉STM32L475
* IDE：Keil MDK5
* RT-Thread源码工程（包括BSP）
* RT-Thread ENV工具

<br>

## 操作步骤

<br>

![步骤一](pics/2020-09-07_115425.png)
### 【步骤一】工程目录里，打开ENV工具

<br><br>

![步骤二](pics/2020-09-07_115631.png)
### 【步骤二】在ENV命令行输入：menuconfig

<br><br>

![步骤三](pics/2020-09-07_115653.png)
### 【步骤三】进入menuconfig主界面，选择【RT-Thread Components】

<br><br>


![步骤四](pics/2020-09-07_115704.png)
### 【步骤四】进入Components菜单项，选择【Device Drivers】

<br><br>

![步骤五](pics/2020-09-07_115743.png)
### 【步骤五】进入Device Drivers菜单项，选择【Using Power Management device drivers】，点击【Save】保存

<br><br>


![步骤六](pics/2020-09-07_120143.png)
### 【步骤六】保存为.config文件（默认，无须修改）

<br><br>


![步骤七](pics/2020-09-07_120209.png)
### 【步骤七】退出menuconfig，可以通过【exit】或【ESC键】，ENV命令行输入：scons --target=mdk5,即可把PM组件加入Keil工程

<br><br>

## 总结

熟悉RT-Thread ENV工具，使用menuconfig配置使能PM组件，使用scons重新构建工程。

# PM2.0新组件设计思路

## 背景

目前已经有一套PM组件，尽量保证兼容的情况下，实现升级与优化，实现PM2.0需求定义。

<br>


## 睡眠与变频模式

### 定义兼容
```
/* All modes used for rt_pm_request() and rt_pm_release() */
enum
{
    /* sleep modes */
    PM_SLEEP_MODE_NONE = 0,
    PM_SLEEP_MODE_IDLE,
    PM_SLEEP_MODE_LIGHT,
    PM_SLEEP_MODE_DEEP,
    PM_SLEEP_MODE_STANDBY,
    PM_SLEEP_MODE_SHUTDOWN,
    PM_SLEEP_MODE_MAX,
};

enum
{
    /* run modes*/
    PM_RUN_MODE_HIGH_SPEED = 0,
    PM_RUN_MODE_NORMAL_SPEED,
    PM_RUN_MODE_MEDIUM_SPEED,
    PM_RUN_MODE_LOW_SPEED,
    PM_RUN_MODE_MAX,
};

```

* 这里的模式定义，还是比较齐全的。因此可以保留。

* 变频功能，改为用户可选

<br>

## 用户实现的接口

* 用户需要适配MCU平台，实现PM框架需要定义的接口，主要为每个模式的实现接口。
* 主要在pm_drv.c中实现

<br>

## PMS线程（工作队列）

有部分外设，关闭时间久，需要多线程通知开关状态，或延时关闭，这时可以通过开启PMS线程实现相应的功能。

* 用户使能PM组件，通过 ENV 工具，menuconfig图形配置方式，开启选中PMS线程宏
* 调用提供的pms工作队列接口，可以实现如延时处理的一些操作


<br>

## 使用说明

* 用户使能PM组件，通过 ENV 工具，menuconfig图形配置方式，开启选中PM组件宏
* 变频功能保留，用户根据实际需求，pm_drv.c 适配平台。
* 各个模式具体的实现，用户可以增加一些处理，如睡眠前与唤醒后的GPIO引脚处理。
* 延时才能关闭的外设或业务操作，可以通过PMS线程配合完成，或用户自定线程实现




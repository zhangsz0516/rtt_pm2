/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2020-09-08     zhangsz      init first
 */

#ifndef __PMS_H__
#define __PMS_H__

#include <rtthread.h>
#include <drivers/pm.h>
#include <rthw.h>

// PM 用户Log 相关的定义，一般为电池管理相关
struct user_log_info {
	uint16_t power; //电量 mAh
	uint16_t voltage; //电压 0.001V
	uint16_t message_cnt; //消息次数
	uint32_t wakeup_cnt; //进入SLEEP之后被唤醒次数
	uint16_t user_dat[4];
};

// PM Log 相关的定义，一般为电池管理
struct pm_log_info {
	uint32_t ticks_in_sleep; //进入SLEEP模式累计时间,tick
	uint16_t run_time; //运行时长
	struct user_log_info user_info;
};

#endif

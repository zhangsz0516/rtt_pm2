/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-02-24     zhangsz   first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "pms.h"

#define DBG_SECTION_NAME    "pms"
#define DBG_LEVEL           DBG_LOG
#include <rtdbg.h>

extern int lpuart1_wakeup_enable(void);
extern int lpuart1_wakeup_disable(void);

/* 消息队列控制块 */
#define PMS_MQ_NUM          20
static struct rt_messagequeue pms_mq;
/* 消息队列中用到的放置消息的内存池 */
static rt_uint32_t pms_mq_buf[PMS_MQ_NUM];

void pm_bsp_enter_idle(struct rt_pm *pm)
{
    __WFI();
}

void pm_bsp_enter_light(struct rt_pm *pm)
{
    if (pm->run_mode == PM_RUN_MODE_LOW_SPEED)
    {
        /* Enter LP SLEEP Mode, Enable low-power regulator */
        HAL_PWR_EnterSLEEPMode(PWR_LOWPOWERREGULATOR_ON, PWR_SLEEPENTRY_WFI);
    }
    else
    {
        /* Enter SLEEP Mode, Main regulator is ON */
        HAL_PWR_EnterSLEEPMode(PWR_MAINREGULATOR_ON, PWR_SLEEPENTRY_WFI);
    }
}

void bsp_DisableSystick(void)
{
  /* Disable SysTick */
  SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
}

void bsp_enableSystick(void)
{
  /* Disable SysTick */
  SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
}

void pm_bsp_enter_deepsleep(struct rt_pm *pm)
{
    /* Enter STOP 2 mode  */
    //bsp_DisableSystick();
    //HAL_SuspendTick();
    //NVIC_ClearPendingIRQ(SysTick_IRQn);
    HAL_PWREx_DisableLowPowerRunMode();
    lpuart1_wakeup_enable();
    __HAL_RCC_WAKEUPSTOP_CLK_CONFIG(RCC_STOP_WAKEUPCLOCK_HSI);
    HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFI);
    /* Re-configure the system clock */
    //HAL_RCC_DeInit();
    //SystemClock_Config();
    //SystemClock_Config_fromSTOP();
    SystemClock_80M();
    lpuart1_wakeup_disable();
    //HAL_ResumeTick();
    //SystemClock_ReConfig(pm->run_mode);
}

void pm_bsp_enter_standby(struct rt_pm *pm)
{
    /* Enter STANDBY mode */
    HAL_PWR_EnterSTANDBYMode();
}

void pm_bsp_enter_shutdown(struct rt_pm *pm)
{
    /* Enter SHUTDOWNN mode */
    HAL_PWREx_EnterSHUTDOWNMode();
}

void pms_debug_disable(void)
{
    HAL_PWREx_DisablePullUpPullDownConfig();
    HAL_DBGMCU_DisableDBGStopMode();
    HAL_DBGMCU_DisableDBGSleepMode();
    HAL_DBGMCU_DisableDBGStandbyMode();
}

void pms_debug_enable(void)
{
    HAL_DBGMCU_EnableDBGStopMode();
    HAL_DBGMCU_EnableDBGSleepMode();
    HAL_DBGMCU_EnableDBGStandbyMode();
}

void pms_mq_init(void)
{
    rt_err_t result;

    /* 初始化消息队列 */
    result = rt_mq_init(&pms_mq,
                        "pms",
                        &pms_mq_buf[0],         /* 内存池指向 msg_pool */
                        sizeof(rt_uint32_t),    /* 每个消息的大小是 4 字节 */
                        PMS_MQ_NUM,             /* 内存池的大小是 msg_buf 的大小 */
                        RT_IPC_FLAG_FIFO);      /* 如果有多个线程等待，按照先来先得到的方法分配消息 */

    if (result != RT_EOK)
    {
        LOG_D("init pms message queue failed.\n");
        return;
    }
}

/* pms event emq test */
static void pms_thread(void* param)
{
    rt_uint32_t mq_cmd = 0x00;

    pms_mq_init();
    //pms_debug_enable();

    LOG_D("pms start!\n");

    while (1)
    {
        if (rt_mq_recv(&pms_mq, &mq_cmd, sizeof(mq_cmd), RT_WAITING_FOREVER) == RT_EOK)
        {
            LOG_D("pms recv!\n");
        }
    }
}

int pms_task_init(void)
{
    rt_thread_t tid;

    tid = rt_thread_create("pms", pms_thread, RT_NULL, 2048, 28, 50);
    rt_thread_startup(tid);
    return 1;
}

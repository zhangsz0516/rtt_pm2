#include <rtthread.h>

static struct rt_mailbox t1_mb;
static struct rt_mailbox t2_mb;
static struct rt_mailbox t3_mb;

static rt_uint8_t msg_buf1[128];
static rt_uint8_t msg_buf2[128];
static rt_uint8_t msg_buf3[128];


#define THREAD1_STACK_SIZE   1024
#define THREAD1_PRIORITY     20
#define THREAD1_TIMESLICE    10

#define THREAD2_STACK_SIZE   1024
#define THREAD2_PRIORITY     20
#define THREAD2_TIMESLICE    10

#define THREAD3_STACK_SIZE   1024
#define THREAD3_PRIORITY     20
#define THREAD3_TIMESLICE    10

static rt_thread_t tid1 = RT_NULL;
static rt_thread_t tid2 = RT_NULL;
static rt_thread_t tid3 = RT_NULL;

void list_mem(void)
{
#ifdef RT_USING_MEMHEAP_AS_HEAP
    extern void list_memheap(void);
    list_memheap();
#else
    rt_size_t total = 0, used = 0, max_used = 0;

    rt_memory_info(&total, &used, &max_used);
    rt_kprintf("total    : %d\n", total);
    rt_kprintf("used     : %d\n", used);
    rt_kprintf("maximum  : %d\n", max_used);
    rt_kprintf("available: %d\n", total - used);
#endif
}

struct mb_msg
{
    rt_uint8_t *data_ptr;
    rt_uint32_t data_size;
};

static void mb_init(void)
{
    rt_err_t result;

    /* 初始化邮箱1 */
    result = rt_mb_init(&t1_mb,
                        "t1mb",
                        &msg_buf1[0],            /* 内存池指向 msg_pool */
                        sizeof(msg_buf1) / 4,    /* 邮箱中的邮件数目，一封邮件占用4字节 */
                        RT_IPC_FLAG_FIFO);       /* 采用FIFO方式进行线程等待 */

    if (result != RT_EOK)
    {
        rt_kprintf("init thread1 mailbox failed.\n");
        return;
    }

    /* 初始化邮箱2 */
    result = rt_mb_init(&t2_mb,
                        "t2mb",
                        &msg_buf2[0],            /* 内存池指向 msg_pool */
                        sizeof(msg_buf2) / 4,    /* 邮箱中的邮件数目，一封邮件占用4字节 */
                        RT_IPC_FLAG_FIFO);       /* 采用FIFO方式进行线程等待 */

    if (result != RT_EOK)
    {
        rt_kprintf("init thread2 mailbox failed.\n");
        return;
    }

    /* 初始化邮箱3 */
    result = rt_mb_init(&t3_mb,
                        "t3mb",
                        &msg_buf3[0],            /* 内存池指向 msg_pool */
                        sizeof(msg_buf3) / 4,    /* 邮箱中的邮件数目，一封邮件占用4字节 */
                        RT_IPC_FLAG_FIFO);       /* 采用FIFO方式进行线程等待 */

    if (result != RT_EOK)
    {
        rt_kprintf("init thread3 mailbox failed.\n");
        return;
    }
    rt_kprintf("mb_init ok.\n");
}

static void thread1_entry(void *param)
{
    struct mb_msg *msg_recv_ptr1;
    struct mb_msg *msg_send_ptr1;
    char sbuf[6] = {'T', 'a', 's', 'k', '1', '.'};

    while(1)
    {
        if (rt_mb_recv(&t1_mb, (rt_ubase_t *)&msg_recv_ptr1, RT_WAITING_FOREVER) == RT_EOK)
        {
            rt_kprintf("thread 1:[recv=%s], print 1.\n", msg_recv_ptr1->data_ptr);
            rt_thread_mdelay(10);
            rt_free(msg_recv_ptr1->data_ptr);
            list_mem();
            rt_free(msg_recv_ptr1);
            list_mem();
            rt_thread_mdelay(500);
            msg_send_ptr1 = (struct mb_msg *)rt_malloc(sizeof(struct mb_msg));
            msg_send_ptr1->data_size = sizeof(sbuf);
            msg_send_ptr1->data_ptr = (rt_uint8_t *)rt_malloc(msg_send_ptr1->data_size);
            rt_memcpy(msg_send_ptr1->data_ptr, sbuf, sizeof(sbuf));
            rt_kprintf("thread 1:[send=%s]\n", msg_send_ptr1->data_ptr);
            rt_mb_send(&t2_mb, (rt_uint32_t)msg_send_ptr1);
        }
    }
}

static void thread2_entry(void *param)
{
    struct mb_msg *msg_recv_ptr2;
    struct mb_msg *msg_send_ptr2;

    char sbuf[6] = {'T', 'a', 's', 'k', '2', '.'};

    while(1)
    {
        if (rt_mb_recv(&t2_mb, (rt_ubase_t *)&msg_recv_ptr2, RT_WAITING_FOREVER) == RT_EOK)
        {
            rt_kprintf("thread 2:[recv=%s], print 2.\n", msg_recv_ptr2->data_ptr);
            rt_thread_mdelay(10);
            rt_free(msg_recv_ptr2->data_ptr);
            list_mem();
            rt_free(msg_recv_ptr2);
            list_mem();
            rt_thread_mdelay(500);
            msg_send_ptr2 = (struct mb_msg *)rt_malloc(sizeof(struct mb_msg));
            msg_send_ptr2->data_size = sizeof(sbuf);
            msg_send_ptr2->data_ptr = (rt_uint8_t *)rt_malloc(msg_send_ptr2->data_size);
            rt_memcpy(msg_send_ptr2->data_ptr, sbuf, sizeof(sbuf));
            rt_kprintf("thread 2:[send=%s]\n", msg_send_ptr2->data_ptr);
            rt_mb_send(&t3_mb, (rt_uint32_t)msg_send_ptr2);
        }
    }
}

static void thread3_entry(void *param)
{
    struct mb_msg *msg_recv_ptr3;
    struct mb_msg *msg_send_ptr3;
    char sbuf[6] = {'T', 'a', 's', 'k', '3', '.'};

    while(1)
    {
        if (rt_mb_recv(&t3_mb, (rt_ubase_t *)&msg_recv_ptr3, RT_WAITING_FOREVER) == RT_EOK)
        {
            rt_kprintf("thread 3:[recv=%s], print 3.\n", msg_recv_ptr3->data_ptr);
            rt_thread_mdelay(10);
            rt_free(msg_recv_ptr3->data_ptr);
            list_mem();
            rt_free(msg_recv_ptr3);
            list_mem();
            rt_thread_mdelay(500);
            msg_send_ptr3 = (struct mb_msg *)rt_malloc(sizeof(struct mb_msg));
            msg_send_ptr3->data_size = sizeof(sbuf);
            msg_send_ptr3->data_ptr = (rt_uint8_t *)rt_malloc(msg_send_ptr3->data_size);
            rt_memcpy(msg_send_ptr3->data_ptr, sbuf, sizeof(sbuf));
            rt_kprintf("thread 3:[send=%s]\n", msg_send_ptr3->data_ptr);
            rt_mb_send(&t1_mb, (rt_uint32_t)msg_send_ptr3);
        }
    }
}

static void task_init(void)
{
    struct mb_msg *msg_send_ptr;
    char sbuf[6] = {'T', 'a', 's', 'k', '1', '.'};
    rt_kprintf("%s: init start!\n", __func__);
    list_mem();
    rt_kprintf("%s: init end!\n", __func__);

    tid1 = rt_thread_create("task1",
                            thread1_entry,
                            RT_NULL,
                            THREAD1_STACK_SIZE,
                            THREAD1_PRIORITY,
                            THREAD1_TIMESLICE);

    
    tid2 = rt_thread_create("task2",
                            thread2_entry,
                            RT_NULL,
                            THREAD2_STACK_SIZE,
                            THREAD2_PRIORITY,
                            THREAD2_TIMESLICE);

    
    tid3 = rt_thread_create("task3",
                            thread3_entry,
                            RT_NULL,
                            THREAD3_STACK_SIZE,
                            THREAD3_PRIORITY,
                            THREAD3_TIMESLICE);

    if (tid1 != RT_NULL)
        rt_thread_startup(tid1);

    if (tid2 != RT_NULL)
        rt_thread_startup(tid2);

    if (tid3 != RT_NULL)
        rt_thread_startup(tid3);

    rt_thread_mdelay(500);
    msg_send_ptr = (struct mb_msg *)rt_malloc(sizeof(struct mb_msg));
    rt_kprintf("%s: rt_malloc 01\n", __func__);
    list_mem();
    rt_kprintf("%s: rt_malloc 01 end\n", __func__);
    msg_send_ptr->data_size = sizeof(sbuf);
    msg_send_ptr->data_ptr = (rt_uint8_t *)rt_malloc(msg_send_ptr->data_size);
    rt_kprintf("%s: rt_malloc 02", __func__);
    list_mem();
    rt_kprintf("%s: rt_malloc 02 end\n", __func__);
    rt_memcpy(msg_send_ptr->data_ptr, sbuf, sizeof(sbuf));
    
    rt_mb_send(&t1_mb, (rt_uint32_t)msg_send_ptr);
    rt_kprintf("task_init ok.\n");
}

int mb_test(void)
{
    mb_init();
    task_init();
    return 1;
}

MSH_CMD_EXPORT(mb_test, mb test);

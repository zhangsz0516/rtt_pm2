/*
 * Copyright (c) 2006-2018, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2018-11-06     SummerGift   first version
 */

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
#include "key.h"

#define DBG_ENABLE
#define DBG_SECTION_NAME    "main"
#define DBG_LEVEL           DBG_LOG
#include <rtdbg.h>

/* defined the LED0 pin: PE7 */
#define LEDR_PIN    GET_PIN(E, 7)
#define LEDG_PIN    GET_PIN(E, 8)
#define LEDB_PIN    GET_PIN(E, 9)

extern void key_gpio_init(void);
int pms_task_init(void);

/* 消息队列控制块 */
static struct rt_messagequeue main_mq;
/* 消息队列中用到的放置消息的内存池 */
static rt_uint8_t msg_buf[128];

void leds_gpio_init(void)
{
    /* set LED0 pin mode to output */
    rt_pin_mode(LEDR_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LEDG_PIN, PIN_MODE_OUTPUT);
    rt_pin_mode(LEDB_PIN, PIN_MODE_OUTPUT);
    rt_pin_write(LEDR_PIN, PIN_HIGH);
    rt_pin_write(LEDG_PIN, PIN_HIGH);
    rt_pin_write(LEDB_PIN, PIN_HIGH);
}

void led_red_on(void)
{
    rt_pin_write(LEDR_PIN, PIN_LOW);
}

void led_red_off(void)
{
    rt_pin_write(LEDR_PIN, PIN_HIGH);
}

void led_blu_on(void *param)
{
    rt_pin_write(LEDB_PIN, PIN_LOW);
}

void led_blu_off(void *param)
{
    rt_pin_write(LEDB_PIN, PIN_HIGH);
}

void led_grn_on(void *param)
{
    rt_pin_write(LEDG_PIN, PIN_LOW);
}

void led_grn_off(void *param)
{
    rt_pin_write(LEDG_PIN, PIN_HIGH);
}

void main_mq_init(void)
{
    rt_err_t result;

        /* 初始化消息队列 */
    result = rt_mq_init(&main_mq,
                        "mainmq",
                        &msg_buf[0],             /* 内存池指向 msg_pool */
                        1,                          /* 每个消息的大小是 1 字节 */
                        sizeof(msg_buf),        /* 内存池的大小是 msg_buf 的大小 */
                        RT_IPC_FLAG_FIFO);       /* 如果有多个线程等待，按照先来先得到的方法分配消息 */

    if (result != RT_EOK)
    {
        LOG_D("init message queue failed.\n");
        return;
    }
}

int main(void)
{
    rt_pm_module_delay_sleep(PM_POWER_ID, 2000);

    key_gpio_init();
    leds_gpio_init();
    main_mq_init();
    pms_task_init();

    while (1)
    {
        /* 从消息队列中接收消息 */
        if (rt_mq_recv(&main_mq, &msg_buf, sizeof(msg_buf), RT_WAITING_FOREVER) == RT_EOK)
        {
            LOG_D("thread1: recv msg from msg queue, the content:%c\n", msg_buf);
        }
    }
}

void dump_system_clk(void)
{
    LOG_D("SysClockFreq=%d, HCLKFreq=%d,PCLK1Freq=%d,PCLK2Freq=%d\n",
            HAL_RCC_GetSysClockFreq(),
            HAL_RCC_GetHCLKFreq(),
            HAL_RCC_GetPCLK1Freq(),
            HAL_RCC_GetPCLK2Freq()
    );
}

MSH_CMD_EXPORT(dump_system_clk, dump system clock);


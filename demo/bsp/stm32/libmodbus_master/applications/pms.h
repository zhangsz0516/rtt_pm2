/*
 * Copyright (c) 2006-2021, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2021-02-24     zhangsz   first version
 */

#include <rtthread.h>
#include <rtdevice.h>

#ifndef __PMS_H__
#define __PMS_H__

void pms_debug_disable(void);
void pms_debug_enable(void);

void pm_bsp_enter_idle(struct rt_pm *pm);
void pm_bsp_enter_light(struct rt_pm *pm);
void pm_bsp_enter_deepsleep(struct rt_pm *pm);
void pm_bsp_enter_standby(struct rt_pm *pm);
void pm_bsp_enter_shutdown(struct rt_pm *pm);

#endif

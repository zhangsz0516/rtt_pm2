#include <rtthread.h>
#include <board.h>

#define DBG_ENABLE
#define DBG_SECTION_NAME    "lpuart"
#define DBG_LEVEL           DBG_LOG
#include <rtdbg.h>

#define SAMPLE_UART_NAME       "lpuart1"

/* Default config for serial_configure structure */
#define LPUART1_CONFIG_DEFAULT             \
{                                          \
    BAUD_RATE_38400, /* 38400 bits/s */    \
    DATA_BITS_8,      /* 8 databits */     \
    STOP_BITS_1,      /* 1 stopbit */      \
    PARITY_NONE,      /* No parity  */     \
    BIT_ORDER_LSB,    /* LSB first sent */ \
    NRZ_NORMAL,       /* Normal mode */    \
    RT_SERIAL_RB_BUFSZ, /* Buffer size */  \
    0                                      \
}

/* Default config for serial_configure structure */
#define LPUART1_BAUD9600_CONFIG            \
{                                          \
    BAUD_RATE_9600,   /* 9600 bits/s */    \
    DATA_BITS_8,      /* 8 databits */     \
    STOP_BITS_1,      /* 1 stopbit */      \
    PARITY_NONE,      /* No parity  */     \
    BIT_ORDER_LSB,    /* LSB first sent */ \
    NRZ_NORMAL,       /* Normal mode */    \
    RT_SERIAL_RB_BUFSZ, /* Buffer size */  \
    0                                      \
}

/* Default config for serial_configure structure */
#define LPUART1_BAUD19200_CONFIG           \
{                                          \
    BAUD_RATE_19200,   /* 19200 bits/s */  \
    DATA_BITS_8,      /* 8 databits */     \
    STOP_BITS_1,      /* 1 stopbit */      \
    PARITY_NONE,      /* No parity  */     \
    BIT_ORDER_LSB,    /* LSB first sent */ \
    NRZ_NORMAL,       /* Normal mode */    \
    RT_SERIAL_RB_BUFSZ, /* Buffer size */  \
    0                                      \
}

/* Default config for serial_configure structure */
#define LPUART1_BAUD115200_CONFIG          \
{                                          \
    BAUD_RATE_115200, /* 115200 bits/s */  \
    DATA_BITS_8,      /* 8 databits */     \
    STOP_BITS_1,      /* 1 stopbit */      \
    PARITY_NONE,      /* No parity  */     \
    BIT_ORDER_LSB,    /* LSB first sent */ \
    NRZ_NORMAL,       /* Normal mode */    \
    RT_SERIAL_RB_BUFSZ, /* Buffer size */  \
    0                                      \
}

extern int lpuart1_wakeup_config(void);

/* 用于接收消息的信号量 */
static struct rt_semaphore rx_sem;
static rt_device_t lp_serial;

static void lpuart_set_config(void)
{
    struct serial_configure config = LPUART1_BAUD9600_CONFIG;
    rt_device_control(lp_serial, RT_DEVICE_CTRL_CONFIG, &config);
}

extern int lpuart1_wakeup_disable(void);

/* 接收数据回调函数 */
static rt_err_t uart_input(rt_device_t dev, rt_size_t size)
{
    /* 串口接收到数据后产生中断，调用此回调函数，然后发送接收信号量 */
    rt_pm_module_request(PM_BOARD_ID, PM_SLEEP_MODE_NONE);
    rt_sem_release(&rx_sem);

    return RT_EOK;
}

static void serial_thread_entry(void *parameter)
{
    char ch;

    while (1)
    {
        /* 从串口读取一个字节的数据，没有读取到则等待接收信号量 */
        while (rt_device_read(lp_serial, -1, &ch, 1) != 1)
        {
            /* 阻塞等待接收信号量，等到信号量后再次读取数据 */
            rt_sem_take(&rx_sem, RT_WAITING_FOREVER);
        }

        /* 读取到的数据通过串口错位输出 */
        ch = ch + 1;
        rt_device_write(lp_serial, 0, &ch, 1);
        //rt_kprintf("wake from lpuart!\n");
        rt_pm_module_release(PM_BOARD_ID, PM_SLEEP_MODE_NONE);
    }
}

int uart_sample(void)
{
    rt_err_t ret = RT_EOK;
    char str[] = "hello lpuart!\r\n";

    /* 查找系统中的串口设备 */
    lp_serial = rt_device_find(SAMPLE_UART_NAME);
    if (!lp_serial)
    {
        LOG_D("find %s failed!\n", SAMPLE_UART_NAME);
        return RT_ERROR;
    }

    /* 初始化信号量 */
    rt_sem_init(&rx_sem, "rx_sem", 0, RT_IPC_FLAG_FIFO);
    /* 以中断接收及轮询发送模式打开串口设备 */

    rt_device_close(lp_serial);
    lpuart_set_config();

    LOG_D("lpuart_set_config ok!\n");

    rt_device_open(lp_serial, RT_DEVICE_FLAG_INT_RX);

    /* 设置接收回调函数 */
    rt_device_set_rx_indicate(lp_serial, uart_input);
    /* 发送字符串 */
    rt_device_write(lp_serial, 0, str, (sizeof(str) - 1));

#if 1
    /* 创建 serial 线程 */
    rt_thread_t thread = rt_thread_create("serial", serial_thread_entry, RT_NULL, 1024, 25, 10);
    /* 创建成功则启动线程 */
    if (thread != RT_NULL)
    {
        rt_thread_startup(thread);
    }
    else
    {
        ret = RT_ERROR;
    }
#endif

    return ret;
}

INIT_APP_EXPORT(uart_sample);
INIT_APP_EXPORT(lpuart1_wakeup_config);

#include <rtthread.h>

#define DBG_ENABLE
#define DBG_SECTION_NAME    "mq.test"
#define DBG_LEVEL           DBG_LOG
#include <rtdbg.h>

static struct rt_messagequeue t1_mq;
static struct rt_messagequeue t2_mq;
static struct rt_messagequeue t3_mq;

static rt_uint8_t msg_buf1[16];
static rt_uint8_t msg_buf2[16];
static rt_uint8_t msg_buf3[16];


#define THREAD1_STACK_SIZE   1024
#define THREAD1_PRIORITY     20
#define THREAD1_TIMESLICE    10

#define THREAD2_STACK_SIZE   1024
#define THREAD2_PRIORITY     20
#define THREAD2_TIMESLICE    10

#define THREAD3_STACK_SIZE   1024
#define THREAD3_PRIORITY     20
#define THREAD3_TIMESLICE    10

static rt_thread_t tid1 = RT_NULL;
static rt_thread_t tid2 = RT_NULL;
static rt_thread_t tid3 = RT_NULL;

static void mq_init(void)
{
    rt_err_t result;

    /* 初始化消息队列 */
    result = rt_mq_init(&t1_mq,
                        "t1mq",
                        &msg_buf1[0],            /* 内存池指向 msg_pool */
                        6,                       /* 每个消息的大小是 6 字节 */
                        sizeof(msg_buf1),        /* 内存池的大小是 msg_buf 的大小 */
                        RT_IPC_FLAG_FIFO);       /* 如果有多个线程等待，按照先来先得到的方法分配消息 */

    if (result != RT_EOK)
    {
        LOG_D("init thread1 message queue failed.\n");
        return;
    }

    /* 初始化消息队列 */
    result = rt_mq_init(&t2_mq,
                        "t2mq",
                        &msg_buf2[0],            /* 内存池指向 msg_pool */
                        6,                       /* 每个消息的大小是 6 字节 */
                        sizeof(msg_buf2),        /* 内存池的大小是 msg_buf 的大小 */
                        RT_IPC_FLAG_FIFO);       /* 如果有多个线程等待，按照先来先得到的方法分配消息 */

    if (result != RT_EOK)
    {
        LOG_D("init thread2 message queue failed.\n");
        return;
    }

    /* 初始化消息队列 */
    result = rt_mq_init(&t3_mq,
                        "t3mq",
                        &msg_buf3[0],            /* 内存池指向 msg_pool */
                        6,                       /* 每个消息的大小是 6 字节 */
                        sizeof(msg_buf3),        /* 内存池的大小是 msg_buf 的大小 */
                        RT_IPC_FLAG_FIFO);       /* 如果有多个线程等待，按照先来先得到的方法分配消息 */

    if (result != RT_EOK)
    {
        LOG_D("init thread3 message queue failed.\n");
        return;
    }
}

static void thread1_entry(void *param)
{
    char buf[6] = { 0 };

    while(1)
    {
        if (rt_mq_recv(&t1_mq, &buf, sizeof(buf), RT_WAITING_FOREVER) == RT_EOK)
        {
            LOG_D("thread 1:[recv=%s], print 1.\n", buf);
            char sbuf[6] = {'T', 'a', 's', 'k', '1', '.'};
            rt_thread_mdelay(500);
            rt_mq_send(&t2_mq, &sbuf, sizeof(sbuf));
        }
    }
}

static void thread2_entry(void *param)
{
    char buf[6] = { 0 };

    while(1)
    {
        if (rt_mq_recv(&t2_mq, &buf, sizeof(buf), RT_WAITING_FOREVER) == RT_EOK)
        {
            LOG_D("thread 2:[recv=%s], print 2.\n", buf);
            char sbuf[6] = {'T', 'a', 's', 'k', '2', '.'};
            rt_thread_mdelay(500);
            rt_mq_send(&t3_mq, &sbuf, sizeof(sbuf));
        }
    }
}

static void thread3_entry(void *param)
{
    char buf[6] = { 0 };

    while(1)
    {
        if (rt_mq_recv(&t3_mq, &buf, sizeof(buf), RT_WAITING_FOREVER) == RT_EOK)
        {
            LOG_D("thread 3:[recv=%s], print 3.\n", buf);
            char sbuf[6] = {'T', 'a', 's', 'k', '3', '.'};
            rt_thread_mdelay(500);
            rt_mq_send(&t1_mq, &sbuf, sizeof(sbuf));
        }
    }
}

static void task_init(void)
{
    char buf[6] = {'T', 'a', 's', 'k', '1', '.'};

    tid1 = rt_thread_create("task1",
                            thread1_entry,
                            RT_NULL,
                            THREAD1_STACK_SIZE,
                            THREAD1_PRIORITY,
                            THREAD1_TIMESLICE);

    
    tid2 = rt_thread_create("task2",
                            thread2_entry,
                            RT_NULL,
                            THREAD2_STACK_SIZE,
                            THREAD2_PRIORITY,
                            THREAD2_TIMESLICE);

    
    tid3 = rt_thread_create("task3",
                            thread3_entry,
                            RT_NULL,
                            THREAD3_STACK_SIZE,
                            THREAD3_PRIORITY,
                            THREAD3_TIMESLICE);

    if (tid1 != RT_NULL)
        rt_thread_startup(tid1);

    if (tid2 != RT_NULL)
        rt_thread_startup(tid2);

    if (tid3 != RT_NULL)
        rt_thread_startup(tid3);

    rt_mq_send(&t1_mq, &buf, sizeof(buf));
}

int mq_test(void)
{
    mq_init();
    task_init();
    return 1;
}

MSH_CMD_EXPORT(mq_test, mq test);

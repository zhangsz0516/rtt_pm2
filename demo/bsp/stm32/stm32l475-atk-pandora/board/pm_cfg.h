#ifndef __PM_CFG_H__
#define __PM_CFG_H__

enum pm_module_id
{
    PM_NONE_ID = 0,
    PM_POWER_ID,
    PM_BOARD_ID,
    PM_KEY0_ID,
    PM_KEY1_ID,
    PM_KEY2_ID,
    PM_MODULE_MAX_ID, /* enum must! */
};

#endif

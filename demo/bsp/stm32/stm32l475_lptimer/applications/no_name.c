#include <rtthread.h>

#define DBG_SECTION_NAME    "noname"
#define DBG_LEVEL           DBG_LOG
#include <rtdbg.h>

 /* led test */
extern void led_red_on(void);
extern void led_red_off(void);
extern void led_grn_on(void);
extern void led_grn_off(void);
extern void led_blu_on(void);
extern void led_blu_off(void);

/* no name thread1 */
static void no_name_task1(void* param)
{
    LOG_D("no name thread1 start!\n");

    while (1)
    {
        led_red_on();
        rt_thread_mdelay(2000);
        led_red_off();
        rt_thread_mdelay(2000);
    }
}

/* no name thread2 */
static void no_name_task2(void* param)
{
    LOG_D("no name thread2 start!\n");

    while (1)
    {
        led_blu_on();
        rt_thread_mdelay(3000);
        led_blu_off();
        rt_thread_mdelay(3000);
    }
}

/* no name thread3 */
static void no_name_task3(void* param)
{
    LOG_D("no name thread3 start!\n");

    while (1)
    {
        led_grn_on();
        rt_thread_mdelay(4000);
        led_grn_off();
        rt_thread_mdelay(4000);
    }
}

void no_name_init(void)
{
    rt_thread_t tid;

    tid = rt_thread_create(RT_NULL, no_name_task1, RT_NULL, 2048, 28, 50);
    rt_thread_startup(tid);

    tid = rt_thread_create(RT_NULL, no_name_task2, RT_NULL, 2048, 28, 50);
    rt_thread_startup(tid);

    tid = rt_thread_create(RT_NULL, no_name_task3, RT_NULL, 2048, 28, 50);
    rt_thread_startup(tid);
}

void no_name_del(void)
{
    rt_thread_t tid;

    tid = rt_thread_find(RT_NULL);
    if (tid != RT_NULL)
    {
        rt_thread_delete (tid);
    }
    else
    {
        LOG_D("Not find thread!");
    }
}

MSH_CMD_EXPORT(no_name_init, no_name_init);
MSH_CMD_EXPORT(no_name_del, no_name_del);

#include <rtthread.h>

#define DBG_ENABLE
#define DBG_SECTION_NAME    "event.test"
#define DBG_LEVEL           DBG_LOG
#include <rtdbg.h>

static struct rt_event t1_evt;
static struct rt_event t2_evt;
static struct rt_event t3_evt;

#define THREAD1_STACK_SIZE   1024
#define THREAD1_PRIORITY     20
#define THREAD1_TIMESLICE    10

#define THREAD2_STACK_SIZE   1024
#define THREAD2_PRIORITY     20
#define THREAD2_TIMESLICE    10

#define THREAD3_STACK_SIZE   1024
#define THREAD3_PRIORITY     20
#define THREAD3_TIMESLICE    10

static rt_thread_t tid1 = RT_NULL;
static rt_thread_t tid2 = RT_NULL;
static rt_thread_t tid3 = RT_NULL;

#define FLAG_RECV       (1<<8)

static void event_init(void)
{
    rt_err_t result;

    /* 初始化事件集 */
    result = rt_event_init(&t1_evt, "event1", RT_IPC_FLAG_FIFO);

    if (result != RT_EOK)
    {
        LOG_D("init thread1 event failed.\n");
        return;
    }

    result = rt_event_init(&t2_evt, "event2", RT_IPC_FLAG_FIFO);

    if (result != RT_EOK)
    {
        LOG_D("init thread2 message queue failed.\n");
        return;
    }

    result = rt_event_init(&t3_evt, "event3", RT_IPC_FLAG_FIFO);

    if (result != RT_EOK)
    {
        LOG_D("init thread3 message queue failed.\n");
        return;
    }
}

static void thread1_entry(void *param)
{
    rt_uint32_t _evt_set = 0x00;

    while(1)
    {
        if (rt_event_recv(&t1_evt, 0xFFFF,
                          RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,
                          RT_WAITING_FOREVER, &_evt_set) == RT_EOK)
        {
            LOG_D("thread 1:[recv=%d], print 1.\n", _evt_set);
        }
    }
}

static void thread2_entry(void *param)
{
    rt_uint32_t _evt_set = 0x00;

    while(1)
    {
        if (rt_event_recv(&t2_evt, 0xFFFF,
                          RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,
                          RT_WAITING_FOREVER, &_evt_set) == RT_EOK)
        {
            LOG_D("thread 2:[recv=%d], print 1.\n", _evt_set);
        }
    }
}

static void thread3_entry(void *param)
{
    rt_uint32_t _evt_set = 0x00;

    while(1)
    {
        if (rt_event_recv(&t3_evt, 0xFFFF,
                          RT_EVENT_FLAG_OR | RT_EVENT_FLAG_CLEAR,
                          RT_WAITING_FOREVER, &_evt_set) == RT_EOK)
        {
            LOG_D("thread 3:[recv=%d], print 1.\n", _evt_set);
        }
    }
}

static void task_init(void)
{
    tid1 = rt_thread_create("task1",
                            thread1_entry,
                            RT_NULL,
                            THREAD1_STACK_SIZE,
                            THREAD1_PRIORITY,
                            THREAD1_TIMESLICE);

    
    tid2 = rt_thread_create("task2",
                            thread2_entry,
                            RT_NULL,
                            THREAD2_STACK_SIZE,
                            THREAD2_PRIORITY,
                            THREAD2_TIMESLICE);

    
    tid3 = rt_thread_create("task3",
                            thread3_entry,
                            RT_NULL,
                            THREAD3_STACK_SIZE,
                            THREAD3_PRIORITY,
                            THREAD3_TIMESLICE);

    if (tid1 != RT_NULL)
        rt_thread_startup(tid1);

    if (tid2 != RT_NULL)
        rt_thread_startup(tid2);

    if (tid3 != RT_NULL)
        rt_thread_startup(tid3);

}

int event_test(void)
{
    event_init();
    task_init();
    return 1;
}

void event_send_task1(void)
{
    rt_event_send(&t1_evt, 0x01);
}

void event_send_task2(void)
{
    rt_event_send(&t2_evt, 0x02);
}

void event_send_task3(void)
{
    rt_event_send(&t3_evt, 0x03);
}

MSH_CMD_EXPORT(event_send_task1, event_send_task1);
MSH_CMD_EXPORT(event_send_task2, event_send_task2);
MSH_CMD_EXPORT(event_send_task3, event_send_task3);
MSH_CMD_EXPORT(event_test, event test);

#include <rtthread.h>

#define DBG_TAG    "demo.timer"
#define DBG_LVL    DBG_LOG
#include <rtdbg.h>

/* 这里 rt_timer_t 是个定时器 指针 */
static rt_timer_t demo3_timer = RT_NULL;
static rt_timer_t demo4_timer = RT_NULL;

static rt_bool_t demo3_timer_init_flag = RT_FALSE;
static rt_bool_t demo4_timer_init_flag = RT_FALSE;

static rt_bool_t demo3_timer_run_flag = RT_FALSE;
static rt_bool_t demo4_timer_run_flag = RT_FALSE;

#define DEMO1_TIMEOUT_TIME              2000
#define DEMO2_TIMEOUT_TIME              5000

/* demo3 timer timeout entry */
static void demo3_timer_timeout(void *param)
{
    LOG_I("%s : enter", __func__);
}

/* demo4 timer timeout entry */
static void demo4_timer_timeout(void *param)
{
    LOG_I("%s : enter", __func__);
}

/* demo3 timer init */
static rt_err_t demo3_timer_init(void)
{
    if (demo3_timer_init_flag == RT_TRUE)
        return RT_EOK;

    demo3_timer = rt_timer_create("demo3_tm", demo3_timer_timeout, RT_NULL,
        DEMO1_TIMEOUT_TIME,
        RT_TIMER_FLAG_PERIODIC | RT_TIMER_FLAG_SOFT_TIMER);

    if (demo3_timer == RT_NULL)
    {
        LOG_E("%s : create error!", __func__);
        return -RT_ERROR;
    }

    demo3_timer_init_flag = RT_TRUE;
    return RT_EOK;
}

/* demo4 timer init */
static rt_err_t demo4_timer_init(void)
{
    if (demo4_timer_init_flag == RT_TRUE)
        return RT_EOK;

    demo4_timer = rt_timer_create("demo4_tm", demo4_timer_timeout, RT_NULL,
        DEMO2_TIMEOUT_TIME,
        RT_TIMER_FLAG_PERIODIC | RT_TIMER_FLAG_SOFT_TIMER);

    if (demo4_timer == RT_NULL)
    {
        LOG_E("%s : create error!", __func__);
        return -RT_ERROR;
    }

    demo4_timer_init_flag = RT_TRUE;

    return RT_EOK;
}

/* demo3 timer Start */
rt_err_t demo3_timer_start(void)
{
    rt_err_t ret = RT_EOK;

    if (demo3_timer_init_flag == RT_FALSE)
    {
        return -RT_ERROR;
    }

    if (demo3_timer_run_flag == RT_FALSE)
    {
        ret = rt_timer_start(demo3_timer);
    }

    if (ret == RT_EOK)
    {
        demo3_timer_run_flag = RT_TRUE;
    }

    return ret;
}

/* demo4 timer Start */
rt_err_t demo4_timer_start(void)
{
    rt_err_t ret = RT_EOK;

    if (demo4_timer_init_flag == RT_FALSE)
    {
        return -RT_ERROR;
    }

    if (demo4_timer_run_flag == RT_FALSE)
    {
        ret = rt_timer_start(demo4_timer);
    }

    if (ret == RT_EOK)
    {
        demo4_timer_run_flag = RT_TRUE;
    }

    return ret;
}

/* demo3 timer Stop*/
rt_err_t demo3_timer_stop(void)
{
    rt_err_t ret = RT_EOK;

    if (demo3_timer_init_flag == RT_FALSE)
    {
        return -RT_ERROR;
    }

    if (demo3_timer_run_flag == RT_TRUE)
    {
        ret = rt_timer_stop(demo3_timer);
    }

    if (ret == RT_EOK)
    {
        demo3_timer_run_flag = RT_FALSE;
    }

    return ret;
}

/* demo4 timer Stop*/
rt_err_t demo4_timer_stop(void)
{
    rt_err_t ret = RT_EOK;

    if (demo4_timer_init_flag == RT_FALSE)
    {
        return -RT_ERROR;
    }

    if (demo4_timer_run_flag == RT_TRUE)
    {
        ret = rt_timer_stop(demo4_timer);
    }

    if (ret == RT_EOK)
    {
        demo4_timer_run_flag = RT_FALSE;
    }

    return ret;
}

MSH_CMD_EXPORT(demo3_timer_init, demo3 timer init);
MSH_CMD_EXPORT(demo4_timer_init, demo4 timer init);

MSH_CMD_EXPORT(demo3_timer_start, demo3 timer start);
MSH_CMD_EXPORT(demo4_timer_start, demo4 timer start);

MSH_CMD_EXPORT(demo3_timer_stop, demo3 timer stop);
MSH_CMD_EXPORT(demo4_timer_stop, demo4 timer stop);

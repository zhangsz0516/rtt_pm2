#include <rtthread.h>

#define DBG_TAG    "demo.timer"
#define DBG_LVL    DBG_LOG
#include <rtdbg.h>

static struct rt_timer demo1_timer = { 0 };
static struct rt_timer demo2_timer = { 0 };

static rt_bool_t demo1_timer_init_flag = RT_FALSE;
static rt_bool_t demo2_timer_init_flag = RT_FALSE;

static rt_bool_t demo1_timer_run_flag = RT_FALSE;
static rt_bool_t demo2_timer_run_flag = RT_FALSE;

#define DEMO1_TIMEOUT_TIME              2000
#define DEMO2_TIMEOUT_TIME              5000

/* demo1 timer timeout entry */
static void demo1_timer_timeout(void *param)
{
    LOG_I("%s : enter", __func__);
}

/* demo2 timer timeout entry */
static void demo2_timer_timeout(void *param)
{
    LOG_I("%s : enter", __func__);
}

/* demo1 timer init */
rt_err_t demo1_timer_init(void)
{
    static rt_uint32_t flag = 0x00;
    if (demo1_timer_init_flag == RT_TRUE)
        return RT_EOK;

    flag = ~(0x02 | 0x04 | 0x08);
    rt_kprintf("flag = 0x%08x\r\n", flag);
    flag = 0x03;
    flag & ~(0x07 << 0 | 0x03 << 6);
    rt_kprintf("flag = 0x%08x\r\n", flag);
    rt_timer_init(&demo1_timer, "demo1_tm", demo1_timer_timeout, RT_NULL,
        DEMO1_TIMEOUT_TIME,
        RT_TIMER_FLAG_PERIODIC | RT_TIMER_FLAG_SOFT_TIMER);

    demo1_timer_init_flag = RT_TRUE;

    return RT_EOK;
}

/* demo2 timer init */
rt_err_t demo2_timer_init(void)
{
    if (demo2_timer_init_flag == RT_TRUE)
        return RT_EOK;

    rt_timer_init(&demo2_timer, "demo2_tm", demo2_timer_timeout, RT_NULL,
        DEMO2_TIMEOUT_TIME,
        RT_TIMER_FLAG_PERIODIC | RT_TIMER_FLAG_SOFT_TIMER);

    demo2_timer_init_flag = RT_TRUE;

    return RT_EOK;
}

/* demo1 timer Start */
rt_err_t demo1_timer_start(void)
{
    rt_err_t ret = RT_EOK;

    if (demo1_timer_init_flag == RT_FALSE)
    {
        return -RT_ERROR;
    }

    if (demo1_timer_run_flag == RT_FALSE)
    {
        ret = rt_timer_start(&demo1_timer);
    }

    if (ret == RT_EOK)
    {
        demo1_timer_run_flag = RT_TRUE;
    }

    return ret;
}

/* demo2 timer Start */
rt_err_t demo2_timer_start(void)
{
    rt_err_t ret = RT_EOK;

    if (demo2_timer_init_flag == RT_FALSE)
    {
        return -RT_ERROR;
    }

    if (demo2_timer_run_flag == RT_FALSE)
    {
        ret = rt_timer_start(&demo2_timer);
    }

    if (ret == RT_EOK)
    {
        demo2_timer_run_flag = RT_TRUE;
    }

    return ret;
}

/* demo1 timer Stop*/
rt_err_t demo1_timer_stop(void)
{
    rt_err_t ret = RT_EOK;

    if (demo1_timer_init_flag == RT_FALSE)
    {
        return -RT_ERROR;
    }

    if (demo1_timer_run_flag == RT_TRUE)
    {
        ret = rt_timer_stop(&demo1_timer);
    }

    if (ret == RT_EOK)
    {
        demo1_timer_run_flag = RT_FALSE;
    }

    return ret;
}

/* demo2 timer Stop*/
rt_err_t demo2_timer_stop(void)
{
    rt_err_t ret = RT_EOK;

    if (demo2_timer_init_flag == RT_FALSE)
    {
        return -RT_ERROR;
    }

    if (demo2_timer_run_flag == RT_TRUE)
    {
        ret = rt_timer_stop(&demo2_timer);
    }

    if (ret == RT_EOK)
    {
        demo2_timer_run_flag = RT_FALSE;
    }

    return ret;
}

MSH_CMD_EXPORT(demo1_timer_init, demo1 timer init);
MSH_CMD_EXPORT(demo2_timer_init, demo1 timer init);

MSH_CMD_EXPORT(demo1_timer_start, demo1 timer start);
MSH_CMD_EXPORT(demo2_timer_start, demo1 timer start);

MSH_CMD_EXPORT(demo1_timer_stop, demo1 timer stop);
MSH_CMD_EXPORT(demo2_timer_stop, demo1 timer stop);
